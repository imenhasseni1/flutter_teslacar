import 'package:flutter/material.dart';

import '../../constanins.dart';

class BatteryStatus extends StatelessWidget {
  const BatteryStatus({
    Key? key,
    required this.constrains,
  }) : super(key: key);

  final BoxConstraints constrains;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "220 mi",
          style: Theme.of(context)
              .textTheme
              .headline3!
              .copyWith(color: Colors.white),
        ),
        Padding(
          padding: const EdgeInsets.all(25.0),
          child: Text(
            "62%",
            style: TextStyle(fontSize: 24,color:  Color(0xFFFF5368)),
          ),
        ),
        Spacer(),
        Text(
          "Charging".toUpperCase(),
          style: TextStyle(fontSize: 20),
        ),
        Text(
          "18 min remaining",
          style: TextStyle(fontSize: 20, color: Color(0xFFFF5368), fontWeight: FontWeight.bold),
        ),
        SizedBox(height: constrains.maxHeight * 0.14),
        DefaultTextStyle(
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left:20.0),
                child: Text("22 mi/hr",style: TextStyle(
                    color:Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                    fontSize: 20
                ),),
              ),
              Padding(
                padding: const EdgeInsets.only(right:20.0),
                child: Text("232 v",style: TextStyle(
                  color:  Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                ),),
              ),
            ],
          ),
        ),
        const SizedBox(height: defaultPadding),
      ],
    );
  }
}